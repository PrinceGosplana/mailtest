//
//  AppDelegate.h
//  MailTest
//
//  Created by Oleksandr Isaiev on 30.03.15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) ViewController * viewController;
@property (strong, nonatomic) UINavigationController * navigationController;

@end

