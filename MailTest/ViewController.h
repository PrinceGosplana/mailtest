//
//  ViewController.h
//  MailTest
//
//  Created by Oleksandr Isaiev on 30.03.15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
@class Reachability;

@interface ViewController : UIViewController <MFMailComposeViewControllerDelegate>
{
    NSMutableData *_responseData;
}

@property (nonatomic, strong) MFMailComposeViewController *mailViewController;

- (BOOL)connected;

@end

