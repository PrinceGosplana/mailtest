//
//  ViewController.m
//  MailTest
//
//  Created by Oleksandr Isaiev on 30.03.15.
//  Copyright (c) 2015 Vinnichenko Dmitry. All rights reserved.
//

#import "ViewController.h"
#import "Reachability.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import "SIAlertView.h"
#import "SVProgressHUD.h"

@interface ViewController ()

@property (nonatomic) BOOL sent;
@end


@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self setMessageSettings];
    _sent = NO;
    UIButton * sendMail = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    sendMail.frame = CGRectMake(0, 0, 120, 40);
    sendMail.center = self.view.center;
    sendMail.backgroundColor = [UIColor orangeColor];
    [sendMail setTitle:@"Send mail" forState:UIControlStateNormal];
    [sendMail setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [sendMail.titleLabel setFont:[UIFont systemFontOfSize:18]];
    [sendMail addTarget:self action:@selector(showMessageAsk) forControlEvents:UIControlEventTouchUpInside];
    sendMail.layer.masksToBounds = YES;
    sendMail.layer.cornerRadius = 6.;
    [self.view addSubview:sendMail];

}

- (void) setMessageSettings {
    [[SIAlertView appearance] setMessageFont:[UIFont systemFontOfSize:18]];
    [[SIAlertView appearance] setMessageColor:[UIColor whiteColor]];
    [[SIAlertView appearance] setCornerRadius:12];
    [[SIAlertView appearance] setShadowRadius:20];
    [[SIAlertView appearance] setViewBackgroundColor:[UIColor blackColor]];
    [[SIAlertView appearance] setCancelButtonColor:[UIColor blackColor]];
    [[SIAlertView appearance] setButtonColor:[UIColor blackColor]];
}

- (void) checkInternetConnection {
    if (![self connected]) {
        NSLog(@"not connection");
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"No internet connection"];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                                  NSLog(@"Ok Clicked");
                              }];
        [alertView show];
    } else {
        NSLog(@"We have connection");
        [self sendMail];
    }
}

- (void) showMessageAsk {
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:nil andMessage:@"Are u show you would like to submit the form?"];
    [alertView addButtonWithTitle:@"OK"
                             type:SIAlertViewButtonTypeCancel
                          handler:^(SIAlertView *alertView) {

                              [self checkInternetConnection];
                          }];
    [alertView addButtonWithTitle:@"Cancel"
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alertView) {
                              
                          }];
    alertView.buttonFont = [UIFont boldSystemFontOfSize:15];
    alertView.transitionStyle = SIAlertViewTransitionStyleBounce;
    [alertView show];
}

- (void) sendMail {
    if ([MFMailComposeViewController canSendMail])
    {
        _mailViewController = [[MFMailComposeViewController alloc] init];
        _mailViewController.mailComposeDelegate = self;
        [_mailViewController setToRecipients:@[@"nevkluchat@gmail.com"]];
        [_mailViewController setSubject:@"Hello from IOS!"];
        [_mailViewController setMessageBody:@"Big beautiful world!" isHTML:NO];
        
        [self presentViewController:_mailViewController animated:YES completion:nil];
        
    } else {
        
        NSLog(@"Device is unable to send email in its current state");
    }

}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    [self showWithProgress];
    if (result) {
        _sent = YES;
    }
    if (error) {
        [self performSelector:@selector(dismiss) withObject:nil afterDelay:0.1f];
        SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"Error" andMessage:@"The form was not sent"];
        [alertView addButtonWithTitle:@"OK"
                                 type:SIAlertViewButtonTypeCancel
                              handler:^(SIAlertView *alertView) {
                                  NSLog(@"Ok Clicked");
                              }];
        [alertView show];

    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - Reachability

- (BOOL)connected
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

#pragma mark - Progress HUD

static float progress = 0.0f;

- (void) showWithProgress {
    progress = 0.0f;
    [SVProgressHUD showProgress:0 status:@"Your form data is now being processed" maskType:SVProgressHUDMaskTypeGradient];
    [self performSelector:@selector(increaseProgress) withObject:nil afterDelay:0.1f];
}

- (void)increaseProgress {
    progress+=0.1f;
    [SVProgressHUD showProgress:progress status:@"Your form data is now being processed" maskType:SVProgressHUDMaskTypeGradient];
    
    if(progress < 0.9f && !_sent)
        [self performSelector:@selector(increaseProgress) withObject:nil afterDelay:0.4f];
    else if (_sent)
        [self performSelector:@selector(dismissSuccess) withObject:nil afterDelay:0.4f];
}

#pragma mark - Dismiss Methods Sample

- (void)dismiss {
    [SVProgressHUD dismiss];
}

- (void)dismissSuccess {
    [SVProgressHUD showSuccessWithStatus:@"Form sent" maskType:SVProgressHUDMaskTypeGradient];
}

- (void)dismissError {
    [SVProgressHUD showErrorWithStatus:@"Failed with Error"];
}


@end
